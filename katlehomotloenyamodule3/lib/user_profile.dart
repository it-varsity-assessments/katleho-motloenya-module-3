import 'dart:io';

import 'package:flutter/material.dart';
import 'package:katlehomotloenyamodule3/dashboard.dart';

import 'main.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';

class UserProfile extends StatefulWidget {
  const UserProfile({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<UserProfile> createState() => _MyUserProfileState();
}

String tempPass = "", confPass = "";

class _MyUserProfileState extends State<UserProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      backgroundColor: Colors.lightBlue[50],
      body: SafeArea(
        minimum: const EdgeInsets.only(top: 10),
        child: Column(
          children: <Widget>[
            Align(
              alignment: const Alignment(-0.93, 0.9),
              child: FloatingActionButton(
                  heroTag: "Back",
                  onPressed: () => {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Dashboard()),
                        )
                      },
                  tooltip: 'Back',
                  child: const Icon(Icons.arrow_back)),
            ),
            CircleAvatar(
                radius: 50,
                backgroundImage: ud.image != null
                    ? FileImage(ud.image!)
                    : const NetworkImage(
                            'https://support.hubstaff.com/wp-content/uploads/2019/08/good-pic.png')
                        as ImageProvider),
            Text(
              ud.name,
              style: const TextStyle(
                  fontSize: 35.0,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Pacifico'),
            ),
            const SizedBox(
              height: 20,
              width: 300,
              child: Divider(
                color: Colors.grey,
              ),
            ),
            Card(
              child: Row(
                children: [
                  const InkWell(
                    splashColor: Colors.grey,
                    child: SizedBox(
                      width: 70,
                      height: 50,
                      child: Icon(Icons.email),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text(ud.email)
                ],
              ),
            ),
            const SizedBox(
              height: 10,
              width: 300,
              child: Divider(
                color: Colors.grey,
              ),
            ),
            Card(
              child: Row(
                children: [
                  const InkWell(
                    splashColor: Colors.grey,
                    child: SizedBox(
                      width: 70,
                      height: 50,
                      child: Icon(Icons.phone),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text(ud.phone)
                ],
              ),
            ),
            const SizedBox(
              height: 10,
              width: 300,
              child: Divider(
                color: Colors.grey,
              ),
            ),
            Card(
              child: Row(
                children: [
                  const InkWell(
                    splashColor: Colors.grey,
                    child: SizedBox(
                      width: 70,
                      height: 100,
                      child: Icon(Icons.location_city),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Flexible(
                      child: Container(
                    padding: const EdgeInsets.only(right: 13.0),
                    child: Text(
                      ud.address,
                      overflow: TextOverflow.fade,
                    ),
                  ))
                ],
              ),
            ),
            const SizedBox(
              height: 10,
              width: 300,
              child: Divider(
                color: Colors.grey,
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => const EditProfile(title: "Edit Profile")),
          );
        },
        tooltip: 'Edit',
        child: const Icon(Icons.edit),
      ),
    );
  }
}

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  ImagePicker picker = ImagePicker();
  File? imgFile = File("");

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future getIma() async {
      await picker.pickImage(source: ImageSource.gallery).then((value) {
        setState(() {
          ud.image = File(value!.path);
        });
      });
      setState(() {});
    }

    return Scaffold(
      backgroundColor: Colors.lightBlue[50],
      body: SingleChildScrollView(
        child: SafeArea(
          minimum: const EdgeInsets.only(top: 10),
          child: Column(
            children: <Widget>[
              Align(
                alignment: const Alignment(-0.93, 0.9),
                child: FloatingActionButton(
                    heroTag: "Back",
                    onPressed: () => {
                          context,
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    const UserProfile(title: 'User Profile')),
                          )
                        },
                    tooltip: 'Back',
                    child: const Icon(Icons.arrow_back)),
              ),
              GestureDetector(
                onTap: () {
                  getIma();
                },
                child: CircleAvatar(
                    radius: 50,
                    backgroundImage: ud.image != null
                        ? FileImage(ud.image!)
                        : const NetworkImage(
                                'https://support.hubstaff.com/wp-content/uploads/2019/08/good-pic.png')
                            as ImageProvider),
              ),
              const SizedBox(
                height: 20,
                width: 50,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 50,
                        child: Icon(Icons.person),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: 250,
                      child: TextFormField(
                        initialValue: ud.name,
                        decoration:
                            const InputDecoration(labelText: "Enter Full Name"),
                        onChanged: (name) {
                          ud.setName(name);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 50,
                        child: Icon(Icons.email),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: 250,
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        initialValue: ud.email,
                        decoration:
                            const InputDecoration(labelText: "Enter Email"),
                        onChanged: (email) {
                          ud.setEmail(email);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 50,
                        child: Icon(Icons.phone),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: 250,
                      child: TextFormField(
                        keyboardType: TextInputType.phone,
                        initialValue: ud.phone,
                        decoration: const InputDecoration(
                            labelText: "Enter Phone Number"),
                        onChanged: (phone) {
                          ud.setPhone(phone);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 100,
                        child: Icon(Icons.location_city),
                      ),
                    ),
                    SizedBox(
                      height: 80,
                      width: 250,
                      child: TextFormField(
                        keyboardType: TextInputType.multiline,
                        maxLines: 10,
                        initialValue: ud.address,
                        decoration:
                            const InputDecoration(labelText: "Enter Address"),
                        onChanged: (address) {
                          ud.setAddress(address);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 50,
                        child: Icon(Icons.password),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: 120,
                      child: TextFormField(
                        initialValue: ud.password,
                        decoration:
                            const InputDecoration(labelText: "Enter Password"),
                        onChanged: (password) {
                          tempPass = password;
                        },
                        obscureText: true,
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    SizedBox(
                      height: 50,
                      width: 140,
                      child: TextFormField(
                        initialValue: ud.password,
                        decoration: const InputDecoration(
                            labelText: "Confirm Password"),
                        onChanged: (password) {
                          confPass = password;
                        },
                        obscureText: true,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  if (tempPass != "" && confPass != "") {
                    if (tempPass.compareTo(confPass) == 0) {
                      ud.password = tempPass;
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const UserProfile(title: 'User Profile')),
                      );
                      _showToast(context, 'Details Saved');
                    } else {
                      _showToast(context, 'Passwords do not match');
                    }
                  } else {
                    _showToast(
                        context, 'Please re-enter password to save details');
                  }
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.lightBlue,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80.0)),
                  textStyle: const TextStyle(color: Colors.white),
                ),
                child: const Text(
                  "SAVE CHANGES",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showToast(BuildContext context, String msg) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text(msg),
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }
}
