import 'package:flutter/material.dart';

import 'dashboard.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'dart:async';

String tempEmail = "";
String tempPass = "";
UserDetails ud = UserDetails();
void main() {
  runApp(const MyApp());
}

class UserDetails {
  File? image;
  String name = '';
  String email = '';
  String phone = '';
  String address = '';
  String password = '';

  void setImage(File? image) {
    this.image = image;
  }

  File getImage() {
    return image!;
  }

  void setName(String name) {
    this.name = name;
  }

  String getName() {
    return name;
  }

  String getEmail() {
    return email;
  }

  void setEmail(String email) {
    this.email = email;
  }

  String getPhone() {
    return phone;
  }

  void setPhone(String phone) {
    this.phone = phone;
  }

  String getPassword() {
    return password;
  }

  void setPassword(String password) {
    this.password = password;
  }

  String getAddress() {
    return address;
  }

  void setAddress(String address) {
    this.address = address;
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const LoginPage(title: 'Login'),
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.lightBlue[50],
      appBar: AppBar(title: const Text("LOGIN")),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: Text(
                  "Welcome.",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.blue[600],
                    fontSize: 30,
                    letterSpacing: 1,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(height: size.height * 0.03),
              Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: Text(
                  "LOGIN",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.blue[600],
                    fontSize: 20,
                    letterSpacing: 1,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.symmetric(horizontal: 40),
                child: TextFormField(
                  onChanged: (value) {
                    tempEmail = value;
                  },
                  decoration:
                      const InputDecoration(labelText: "Please enter email"),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.symmetric(horizontal: 40),
                child: TextFormField(
                  onChanged: (value) {
                    tempPass = value;
                  },
                  decoration:
                      const InputDecoration(labelText: "Please enter password"),
                  obscureText: true,
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                margin:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: GestureDetector(
                  onTap: () => {},
                  child: const Text(
                    "Forgot your password?",
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.black26,
                    ),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 5),
                child: ElevatedButton(
                  onPressed: () {
                    if (tempPass != "" || tempEmail != "") {
                      if (tempPass == ud.password && tempEmail == ud.email) {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Dashboard()),
                        );
                      } else {
                        _showToast(context, 'User details incorrect, Please check them or register them');
                      }
                    } else {
                      _showToast(context, 'User details needed');
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.lightBlue,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 50, vertical: 20),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                    textStyle: const TextStyle(color: Colors.white),
                  ),
                  child: const Text(
                    "LOGIN",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                margin:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: GestureDetector(
                  onTap: () => {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              const RegisterPage(title: 'Register')),
                    )
                  },
                  child: const Text(
                    "Don't have a Login details? Create them",
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.black26,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<RegisterPage> createState() => _RegisterState();
}

void _showToast(BuildContext context, String msg) {
  final scaffold = ScaffoldMessenger.of(context);
  scaffold.showSnackBar(
    SnackBar(
      content: Text(msg),
      action:
          SnackBarAction(label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
    ),
  );
}

class _RegisterState extends State<RegisterPage> {
  ImagePicker picker = ImagePicker();
  File? imgFile = File("");
  String tempPass = "", confPass = "";
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future getIma() async {
      await picker.pickImage(source: ImageSource.gallery).then((value) {
        setState(() {
          ud.image = File(value!.path);
        });
      });

      //_imgFile!.saveTo("asset/");
      //ud.image = imgFile!.path;
      setState(() {});
    }

    return Scaffold(
      backgroundColor: Colors.lightBlue[50],
      appBar: AppBar(title: const Text("REGISTER")),
      body: SingleChildScrollView(
        child: SafeArea(
          minimum: const EdgeInsets.only(top: 10),
          child: Column(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  getIma();
                },
                child: CircleAvatar(
                    radius: 50,
                    backgroundImage: ud.image != null
                        ? FileImage(ud.image!)
                        : const NetworkImage(
                                'https://support.hubstaff.com/wp-content/uploads/2019/08/good-pic.png')
                            as ImageProvider),
              ),
              const SizedBox(
                height: 20,
                width: 50,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 50,
                        child: Icon(Icons.person),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: 250,
                      child: TextFormField(
                        initialValue: ud.name,
                        decoration:
                            const InputDecoration(labelText: "Enter Full Name"),
                        onChanged: (name) {
                          ud.setName(name);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 50,
                        child: Icon(Icons.email),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: 250,
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        initialValue: ud.email,
                        decoration:
                            const InputDecoration(labelText: "Enter Email"),
                        onChanged: (email) {
                          ud.setEmail(email);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 50,
                        child: Icon(Icons.phone),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: 250,
                      child: TextFormField(
                        initialValue: ud.phone,
                        decoration: const InputDecoration(
                            labelText: "Enter Phone Number"),
                        onChanged: (phone) {
                          ud.setPhone(phone);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 100,
                        child: Icon(Icons.location_city),
                      ),
                    ),
                    SizedBox(
                      height: 80,
                      width: 250,
                      child: TextFormField(
                        keyboardType: TextInputType.multiline,
                        maxLines: 10,
                        initialValue: ud.address,
                        decoration:
                            const InputDecoration(labelText: "Enter Address"),
                        onChanged: (address) {
                          ud.setAddress(address);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 50,
                        child: Icon(Icons.password),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: 120,
                      child: TextFormField(
                        initialValue: ud.password,
                        decoration:
                            const InputDecoration(labelText: "Enter Password"),
                        onChanged: (password) {
                          tempPass = password;
                        },
                        obscureText: true,
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    SizedBox(
                      height: 50,
                      width: 140,
                      child: TextFormField(
                        initialValue: ud.password,
                        decoration: const InputDecoration(
                            labelText: "Confirm Password"),
                        onChanged: (password) {
                          confPass = password;
                        },
                        obscureText: true,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  if (tempPass.compareTo(confPass) == 0) {
                    ud.setPassword(tempPass);
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              const LoginPage(title: 'Login')),
                    );
                    _showToast(context, 'Registered. Please Log In');
                  } else {
                    _showToast(context, 'Passwords do not match');
                  }
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.lightBlue,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80.0)),
                  textStyle: const TextStyle(color: Colors.white),
                ),
                child: const Text(
                  "REGISTER",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
